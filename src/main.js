import HMR from '@roxi/routify/hmr'
import App from './App.svelte';
import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";

// if(!window.location.host.startsWith('localhost:')){
  Sentry.init({
    dsn: "https://3f554cc33c364fbe8b597e8bb7da6970@o560563.ingest.sentry.io/5924636",
    integrations: [new Integrations.BrowserTracing()],
  
    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
  });
// }


const app = HMR(App, { target: document.body }, 'routify-app')

export default app;