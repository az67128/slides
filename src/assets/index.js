import Add from './Add.svelte'
import Avatar from './Avatar.svelte'
import Close from './Close.svelte'
import Download from './Download.svelte'
import Edit from './Edit.svelte'
import Help from './Help.svelte'
import Logout from './Logout.svelte'
import Menu from './Menu.svelte'
import Share from './Share.svelte'
import Slides from './Slides.svelte'
import Preview from './Preview.svelte'
import Trash from './Trash.svelte'
import Chat from './Chat.svelte'

export default {
    Add, Avatar, Close, Download, Edit, Help, Logout, Menu, Share, Slides, Preview, Trash, Chat
}