import text from "./text.svelte";
import select from "./select.svelte";
import longtext from "./longtext.svelte";
import image from "./image.svelte";
import multiimage from "./multiimage.svelte";
import date from './date.svelte'
import videolink from './videolink.svelte'
import audiolink from './audiolink.svelte'
import document from './document.svelte'
import html from './html.svelte'

export default { text, select, longtext, image, multiimage, date, videolink, audiolink, document, html };
