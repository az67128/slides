import { translation } from "store/lang";
const dict ={
  ADD_IMAGE: { en: "Add image", ru: "Добавить изображение" },
  ADD_DOCUMENT: { en: "Upload document or select from uploaded", ru: "Загрузить документ или выбрать из загруженных" },
  DAYS_OF_WEEK: {
    en: [
      ["Sunday", "Sun"],
      ["Monday", "Mon"],
      ["Tuesday", "Tue"],
      ["Wednesday", "Wed"],
      ["Thursday", "Thu"],
      ["Friday", "Fri"],
      ["Saturday", "Sat"],
    ],
    ru: [
      ["Воскресенье", "Вс"],
      ["Понедельник", "Пн"],
      ["Вторник", "Вт"],
      ["Среда", "Ср"],
      ["Четверг", "Чт"],
      ["Пятница", "Пт"],
      ["Суббота", "Сб"],
    ],
  },
  MONTH_OF_YEAR: {
    en: [
      ['January', 'Jan'],
      ['February', 'Feb'],
      ['March', 'Mar'],
      ['April', 'Apr'],
      ['May', 'May'],
      ['June', 'Jun'],
      ['July', 'Jul'],
      ['August', 'Aug'],
      ['September', 'Sep'],
      ['October', 'Oct'],
      ['November', 'Nov'],
      ['December', 'Dec']
    ],
    ru: [
      ["Январь", "Янв"],
      ["Февраль", "Фев"],
      ["Март", "Мар"],
      ["Апрель", "Апр"],
      ["Май", "Май"],
      ["Июнь", "Июн"],
      ["Июль", "Июл"],
      ["Август", "Авг"],
      ["Сентябрь", "Сен"],
      ["Октябрь", "Окт"],
      ["Ноябрь", "Ноя"],
      ["Декабрь", "Дек"],
    ],
  },
  SELECT_IMAGE: {
    en: "Select an image for upload",
    ru: "Выберите изображение для загрузки",
  },
  SELECT_FROM_GALERY: {
    en: "Select from galery",
    ru: "Выбрать из галереи",
  },
  UPLOADING:{en:'Uploading', ru:'Загружаем'},
  SELECT_OPTION: {en:'Select option', ru:'Выберите опцию'},
  LINK_TEXT:{en:'Link text',ru:'Текст ссылки'},
  FILE_URL:{en:'File url',ru:'Ссылка на файл'},
  URL:{en:"Url", ru:'Ссылка'},
  FILE:{en:"File", ru:"Файл"}
};
const text = translation(dict);
export default text;