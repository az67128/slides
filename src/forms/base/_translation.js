import { translation } from "store/lang";
const dict ={
  UPLOAD: { en: "Upload", ru: "Загрузить" },
  LIB: { en: "Library", ru: "Библиотека" },
  LOADING: { en: "Loading", ru: "Загружаем" },
  SELECT_IMAGE: {
    en: "Select an image for upload",
    ru: "Выберите изображение для загрузки",
  },
  SELECT_DOCUMENT: {
    en: "Select a document for upload. .doc, .docx, .xls, .xlsx, .pdf are supported",
    ru: "Выберите документ для загрузки. Поддерживаются форматы .doc, .docx, .xls, .xlsx, .pdf",
  },
  NO_IMAGES: {
    en: "You don't have images yet",
    ru: "У вас еще нет загруженных изображений",
  },
  NO_DOCUMENTS: {
    en: "You don't have documents yet",
    ru: "У вас еще нет загруженных документов",
  },
  CANCEL: { en: "Cancel", ru: "Отмена" },
  SELECT: { en: "Select", ru: "Выбрать" },
  STOCK: { en: "Photobank", ru: "Фотобанк" },
  NO_IMAGES_FOUND: { en: "Nothing found", ru: "Ничего не нашлось" },
  SEARCH_IMAGE_PLACEHOLDER: { en: "Search for photos", ru: "Поиск фото" },
  TYPE_TO_SEARCH: { en: "Type something to search", ru: "Введите запрос для поиска фотографий" },
};
const text = translation(dict);
export default text;