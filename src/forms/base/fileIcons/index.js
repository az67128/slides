import Doc from "./Doc.svelte";
import File from "./File.svelte";
import Pdf from "./Pdf.svelte";
import Xls from "./Xls.svelte";

export default {
  doc: Doc,
  docx: Doc,
  file: File,
  pdf: Pdf,
  xls: Xls,
  xlsx: Xls,
};
