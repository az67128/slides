export default [
    {border:'#03a8b9', background:'rgba(3,168,185, 0.05)', },
    {border: '#0383b9', background:'rgba(3,131,185, 0.05)', },
    {border: '#1203b9', background:'rgba(18,3,185, 0.05)', },
    {border: '#8803b9', background:'rgba(136,3,185, 0.05)',},
    {border: '#b903a7', background:'rgba(185,3,167, 0.05)',},
    {border: '#b90364', background:'rgba(185,3,100, 0.05)',},
    {border: '#b90310', background:'rgba(185,3,16, 0.05)',},
    {border: '#b95203', background:'rgba(185,82,3, 0.05)',},
    {border: '#f6c244', background:'rgba(246,194,68, 0.05)',},
    {border: '#222222', background:'rgba(34,34,34, 0.05)'}
  ]