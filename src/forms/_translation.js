import { translation } from "store/lang";
const dict = {
  PROJECT_NAME: {
    en: "Project name",
    ru: "Название проекта",
  },
  PRIVATE: { en: "Public", ru: "Частная" },
  PUBLIC: { en: "Private", ru: "Публичная" },
  PRIVATE_HINT: {
    en: "The presentation is visible only to you",
    ru: "Презентация видна только вам",
  },
  PUBLIC_HINT: {
    en: "The presentation is visible to anyone who has a link",
    ru: "Презентация видна всем, у кого есть ссылка",
  },
};
const text = translation(dict);
export default text;
