import { apiUrl } from "config";

const getApiPath = (path) => {
  return `${apiUrl.endsWith("/") ? apiUrl.slice(0, -1) : apiUrl}${
    path.startsWith("/") ? path : "/" + path
  }`;
};

const callApi = (path, options) => {
  const url = getApiPath(path);
  const responseHeaders = {};
  return fetch(url, {
    credentials: "include",
    ...options,
    headers: options.headers
      ? {
          ...options.headers,
        }
      : {
          "Content-type": "application/json; charset=utf-8",
        },
  }).then((res) => {
    if (options.withHeaders) {
      for (const [name, value] of res.headers.entries()) {
        responseHeaders[name] = value;
      }
    }
    switch (options.format) {
      case "json":
        return res.json();
      case "text":
        return res.text();
      case "blob":
        return res.blob();
      case "formData":
        return res.formData();
      case "arrayBuffer":
        return res.arrayBuffer();
      default:
        return res.json();
    }
  });
};

const api = {
  get: (path, options = {}) => callApi(path, { ...options, method: "GET" }),
  post: (path, options = {}) =>
    callApi(path, {
      ...options,
      method: "POST",
      body: JSON.stringify(options.body || ""),
    }),
  patch: (path, options = {}) =>
    callApi(path, {
      ...options,
      method: "PATCH",
      body: JSON.stringify(options.body || ""),
    }),
  put: (path, options = {}) =>
    callApi(path, {
      ...options,
      method: "PUT",
      body: JSON.stringify(options.body || ""),
    }),
  delete: (path, options = {}) =>
    callApi(path, {
      ...options,
      method: "DELETE",
      format: 'text',
      body: JSON.stringify(options.body || ""),
    }),
  upload: (path, options = {}) =>
    callApi(path, {
      ...options,
      method: "POST",
      body: options.body,
      headers: { ...(options.headers || {}) },
    }),
};

export default api;
