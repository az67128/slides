import { translation } from "store/lang";
const dict = {
  SHOW_ALL: { en: "Show all", ru: "Показать все" },
  NEW_PROJECT_NAME: {
    en: "The name of new file",
    ru: "Название нового файла",
  },
  TYPE_NAME: { en: "Type the name", ru: "Введите название" },
  CANCEL: { en: "Cancel", ru: "Отмена" },
  CREATE: { en: "Create", ru: "Cоздать" },
  NEW_PROJECT: { en: "New slide", ru: "Новая презентация" },

  EDIT: { en: "Edit", ru: "Редактировать" },
  PREVIEW: { en: "Preview", ru: "Просмотр" },
  MY_SLIDES: { en: "My slides", ru: "Мои презентации" },
  DOWNLOAD: { en: "Download", ru: "Скачать" },
  SHARE: { en: "Share", ru: "Поделиться" },
  LOGOUT: { en: "Logout", ru: "Выйти из аккаунта" },
  SHARE_TITLE: { en: "Slides generator", ru: "Генератор презентаций" },
  DELETE: { en: "Delete", ru: "Удалить" },
  CONFIRM_DELETE: {en:'Do you want to delete this project?', ru:'Вы действительно хотите удалить проект?'},
  SHARE_VK: {en:'Share via Vkontakte', ru:'Поделиться вконтакте'},
  SHARE_FB: {en:'Share via Facebook', ru:'Поделиться в фейсбук'},
  SHARE_OK: {en:'Share via Odnoklassniki', ru:'Поделиться в одноклассниках'},
  SHARE_TWITTER: {en:'Share via Twitter', ru:'Поделиться в twitter'},
  SHARE_TG: {en:'Share via Telegram', ru:'Поделиться в телеграм'},
  SHARE_LINK: {en:'Share via link', ru:'Поделиться ссылкой'},
  PROJECT_IS_PRIVATE: {en:'The project visible only to you. Make it public?', ru:'Проект виден только вам, сделать публичным?'},
  MAKE_PUBLIC:{en:'Make publick', ru:'Сделать публичным'},
  CONTINUE_PRIVATE:{en:"Stay private", ru:'Оставить приватным'},
  SERVER_ERROR: {en:'Server error', ru:'На сервере произошла ошибка'},
  SLIDES_COPIED:{en:'Slides copied into your account', ru:'Презентация скопирована в ваш аккаунт'},
  SUPPORT: {en:'Support', ru:'Поддержка'}
};
const text = translation(dict);
export default text
