import { toast } from "@zerodevx/svelte-toast";
import { get } from 'svelte/store';
import { translation } from "store/lang";
const dict = {
  COPIED:{en:'Copied!', ru:'Скопировано!'},
  COPY_FAILED:{en:'Copy failed', ru:'Копирование не удалось'}
};
const text = get(translation(dict));
export function toastSuccess(text) {
  toast.push(text, {
    theme: { //TODO - design for successful toast
      '--toastBackground': '#4caf50',
      '--toastColor': 'white',
      '--toastProgressBackground': 'white',
    }
  });
}
export function toastFail(text) {
  toast.push(text);
}

function fallbackCopyTextToClipboard(textToCopy) {
  var textArea = document.createElement("textarea");
  textArea.value = textToCopy;

  // Avoid scrolling to bottom
  textArea.style.top = "0";
  textArea.style.left = "0";
  textArea.style.position = "fixed";

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand("copy");
    var msg = successful ? "successful" : "unsuccessful";
    console.log("Fallback: Copying text command was " + msg);
    if (successful) {
      toastSuccess(text("COPIED"));
    } else {
      toastFail(text("COPY_FAILED"));
    }
  } catch (err) {
    console.error("Fallback: Oops, unable to copy", err);
    toastFail(text("COPY_FAILED"));
  }

  document.body.removeChild(textArea);
}
export function copyTextToClipboard(textToCopy) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(textToCopy);
    return;
  }
  navigator.clipboard.writeText(textToCopy).then(
    function () {
      console.log("Async: Copying to clipboard was successful!");
      toastSuccess(text("COPIED"));
    },
    function (err) {
      console.error("Async: Could not copy text: ", err);
      toastFail(text("COPY_FAILED"));
    }
  );
}


export function pannable(node, project) {
  let x;
  let y;
  let isConcidirent = true;
  let startTime;
  let startTimeoutId;

  function handleMousedown(event) {
    startTime = performance.now();
    isConcidirent = true;
    const targetevent = event.touches ? event.touches[0] : event;
    x = targetevent.clientX;
    y = targetevent.clientY;
    startTimeoutId = setTimeout(() => {
      event.preventDefault();
      node.dispatchEvent(
        new CustomEvent("panstart", {
          detail: { x, y, project },
        })
      );
    }, event.touches ? 500: 300);

    document.addEventListener("mousemove", handleMousemove);
    document.addEventListener("mouseup", handleMouseup);
    document.addEventListener("touchmove", handleMousemove, {
      passive: false,
    });
    document.addEventListener("touchend", handleMouseup);
  }

  function handleMousemove(event) {
    if (startTimeoutId) {
      clearTimeout(startTimeoutId)
      startTimeoutId = null;
      if (performance.now() - startTime < (event.touches ? 200: 100)) {
        handleMouseup();
        return;
      } 
    }
    event.preventDefault();
    const targetevent = event.touches ? event.touches[0] : event;

    const dx = targetevent.clientX - x;
    const dy = targetevent.clientY - y;
    x = targetevent.clientX;
    y = targetevent.clientY;

    node.dispatchEvent(
      new CustomEvent("panmove", {
        detail: { x, y, dx, dy, project },
      })
    );
  }

  function handleMouseup(event) {

    node.dispatchEvent(
      new CustomEvent("panend", {
        detail: { x, y, project },
      })
    );

    document.removeEventListener("mousemove", handleMousemove);
    document.removeEventListener("mouseup", handleMouseup);
    document.removeEventListener("touchmove", handleMousemove);
    document.removeEventListener("touchend", handleMouseup);
  }

  node.addEventListener("mousedown", handleMousedown);
  node.addEventListener("touchstart", handleMousedown);

  return {
    destroy() {
      node.removeEventListener("mousedown", handleMousedown);
      node.removeEventListener("touchstart", handleMousedown);
    },
  };
}