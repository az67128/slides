import { translation } from "store/lang";
const dict = {
  FILL_LOGIN: {
    en: "Enter you email",
    ru: "Укажите ваш email",
  },
  FILL_PASS: { en: "Enter your password", ru: "Введите пароль" },
  NEED_ACTIVATE: {
    en: "You must activate your account first",
    ru: "Подтвердите свой email для входа",
  },
  FILL_FORM: { en: "fill all form items", ru: "Заполните все поля" },
};
const text = translation(dict);
export default text;
