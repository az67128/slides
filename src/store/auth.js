import { writable, get } from 'svelte/store';
import {apiUrl} from 'config'
import { toast } from "@zerodevx/svelte-toast";
import text from './_translation.js'
import { customAlphabet } from 'nanoid/non-secure'
import * as Sentry from "@sentry/browser";

const nanoid = customAlphabet('1234567890abcdef', 10)

const checkApiAuth = () => {
    return fetch(`${apiUrl}/api/v1/profile/`, {credentials:'include'}).then(res=>{
        if(res.status===200) return res.json()
        throw new Error('Not authorized')
    })
};

const logout = () => {
    return fetch(`${apiUrl}/api/auth/logout/`, {credentials:'include'}).then(res=>res.json())
};

export const user = (() => {
    const { update, set, subscribe } = writable({ 
        isLoading: true, 
        userData: null,
    });
    const checkAuth = () => {
        set({ isLoading: true, userData: null });
        checkApiAuth()
            .then((res) => {
                Sentry.setUser({ username: res.username });
                set({ isLoading: false, userData: res })
            })
            .catch((err) => {
                set({ isLoading: false, userData: null })
                
            });
    };
    checkAuth();
    return {
        subscribe,
        checkAuth,
        logout: () => {
            logout().finally(() => set({ isLoading: false, userData: null }));
        },
        login: (email, password) => {
            let _user, _email, _password;
            _user = get(user)
            if (!_user.needConfirmation) {
                if (email === '') return toast.push('login is empty')
                else _email = email;
                if (password === '') return toast.push('password is empty')
                else _password = password;
            } else {
                _email = _user.email;
                _password = _user.password;
            }

            try {
                const data = fetch(`${apiUrl}/api/auth/email/login/`, {
                    method: 'POST', 
                    credentials:'include',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({
                        email: _email, 
                        password: _password,
                    })})
                    .then(res => res.json())
                    .then(response => {
                        if (response?.msg === 'ok' ) {
                            set({userData: {} })
                        } else if (response?.msg === "you must activate your account first") {
                            if (_user.needConfirmation) toast.push(get(text)("NEED_ACTIVATE"));
                            else set({needConfirmation: true, email:_email, password:_password})
                        } else if (response?.msg || response?.detail || response?.email || response?.password) { // #FIXME
                            response?.email && response.email.forEach(msg => toast.push(msg)) 
                            response?.password && response.password.forEach(msg => toast.push(msg)) 
                            response?.msg && toast.push(response.msg);
                            response?.detail && toast.push(response.detail);
                        } else throw new Error(JSON.stringify(response));
                    })
            } catch(err) {
                console.error(err)
            }
        },
        signup: (userData) => {
            
            if (Object.values(userData).includes('')) return toast.push(get(text)("FILL_FORM"))
            try {
                const data = fetch(`${apiUrl}/api/auth/email/registration/`, {
                    method: 'POST', 
                    credentials:'include',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify({
                        email: userData.email, 
                        password: userData.password,
                        username: nanoid(),
                        first_name: userData.firstName,
                        last_name: userData.lastName,
                    })})
                    .then(res=>res.json())
                    .then(response => {
                        if (response.msg === 'ok' ) {
                            user.login(userData.email, userData.password);
                        } else if (response?.msg || response?.detail || response?.email || response?.password) { // #FIXME
                            response?.email && response.email.forEach(msg => toast.push(msg)) 
                            response?.password && response.password.forEach(msg => toast.push(msg)) 
                            response?.msg && toast.push(response.msg);
                            response?.detail && toast.push(response.detail);
                        } else throw new Error(JSON.stringify(response));
                    });                
            } catch(err) {
                console.error(err)
            }
        }
    };
})();
