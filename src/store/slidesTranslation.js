export default {
  OWN_OPTION: { en: "Own Option", ru: "Свой вариант" },
  "Рекламная кампания": { en: "Advertising campaign" },
  "Согласование рекламного ролика": { en: "Commercial approval" },
  "Раскадровка видео": { en: "Video storyboard" },
  Название: { en: "Name" },
  "Напишите название рекламы/ролика/видео. Например: Ночной всадник на Ладе. Не более 50 символов":
    {
      en: "Write the name of your ad  / video. For example: Night Rider on Lada. 50 characters max",
    },
  "Фон слайда": { en: "Slide background" },
  "Загрузите свою картинку или воспользуйтесь картиной из галереи": {
    en: "Upload your picture or use an image from the gallery",
  },
  Логотип: { en: "Logo" },
  "Логотип компании": { en: "Company `s logo" },
  Кадр: { en: "Frame" },
  "Описание кадра": { en: "Frame description" },
  "Коротко расскажите, что происходит в кадре, покажите акцент. Не более 100 символов":
    {
      en: "Briefly tell what is happening in the frame, show the accent. No more than 100 characters",
    },
  "Загрузите свою картинку или воспользуйтесь картиной из стоковой галереи": {
    en: "Upload your picture or use an image from the gallery",
  },
  "Название компании": { en: "Company name" },
  "Напишите название компании, которая делает рекламу. Например: Данон. Не более 50 символов":
    {
      en: "Write the name of the company that making the ad. For example: Danone. 50 characters max",
    },
  "Загрузите свою картинку или воспользуйтесь изображением из галереи": {
    en: "Upload your picture or use an image from the gallery",
  },
  Слоган: { en: "Tagline" },
  "Напишите слоган компании, например: Быть лучше каждый день. Не более 100 символов":
    {
      en: "Write a company slogan, for example: Be better every day. 100 characters max",
    },
  "Название рекламы": { en: "Name of advertisement" },
  "Введите свой вариант, не более 50 символов": {
    en: "Enter your option. 50 characters max",
  },
  Сообщение: { en: "Message" },
  "Что рекламируем": { en: "What we advertise" },
  "Коротко расскажите, что из себя представляет рекламируемый продукт. Не более 100 символов":
    {
      en: "Briefly describe what the advertised product is. 100 characters max",
    },
  "Сильные стороны продукта": { en: "Product strengths" },
  "Коротко опишите сильные стороны вашего рекламного продукта. Не более 100 символов":
    {
      en: "Briefly describe the strengths of your ad product. 100 characters max",
    },
  Идея: { en: "Idea" },
  "Название идеи": { en: "Idea name" },
  "Напишите название идеи или ее номер, например: Идея 1. Не более 50 символов":
    {
      en: "Write the name of the idea or its number, for example: Idea 1. 50 characters max",
    },
  Раскадровка: { en: "Storyboard" },
  "Зугрузите картинки, которые могут лучше представить вашу идею": {
    en: "Upload pictures that better represent your idea",
  },
  "Рекламный сценарий": { en: "Advertising script" },
  "Название сценария": { en: "Scenario name" },
  "Придумайте название для вашего сценария. Не более 100 символов": {
    en: "Come up with a title for your script. 100 characters max",
  },
  Сценарий: { en: "Scenario" },
  "Опишите рекламный сценарий. Не более 2000 символов": {
    en: "Describe your ad script. 2000 characters max",
  },
  Финализирующий: { en: "Finalizing" },
  "Введите резюмирующую информацию": { en: "Enter summary information" },
  "Коротко финализируйте свою идею или завершите презентацию благодарностью за внимание. Не более 100 символов":
    {
      en: "Finalize your idea shortly or end your presentation with a thank you for your attention. 100 characters max",
    },
  "Загрузите свою картинку или воспользуйтесь картинкой из галереи": {
    en: "Upload your picture or use an image from the gallery",
  },
  Где: { en: "Where" },
  "Впишите свой вариант": { en: "Enter your option" },
  Когда: { en: "When" },
  "Укажете дату согласования": { en: "Indicate the date of approval" },
  "Загрузите свою картинку или воспользуйтесь картинкой из галереи": {
    en: "Upload your picture or use an image from the gallery",
  },
  "Список участников": { en: "List of participants" },
  "Напишите название участника. Например: Билайн": {
    en: "Write the name of the participant. For example: Beeline",
  },
  "Загрузите свою картинку или воспользуйтесь картинкой из стоковой галереи": {
    en: "Upload your picture or use an image from the gallery",
  },
  Участники: { en: "Participants" },
  "Введите ФИ участников и их должности. Пример: Аслексей Васин - Арт Директор":
    {
      en: "Enter the names of the participants and their positions. Example: Asleksey Vasin - Art Director",
    },
  Имя: { en: "Name" },
  Должность: { en: "Position" },
  Роли: { en: "Roles" },
  "Напишите название роли. Например: водитель. Не более 50 символов": {
    en: "Write the name of the role. For example: driver. 50 characters max",
  },
  "Имя актера": { en: "Actor name" },
  "Напишите ФИ актера. Например: Аркадий Мухин": {
    en: "Write the name of the actor. For example: Arkady Mukhin",
  },
  "Фото актера": { en: "Photo of the actor" },
  "Ссылка на видео актера": { en: "Link to the actor's video" },
  "Добавьте ссылку на видео актера": { en: "Add a link to the actor's video" },
  "Костюм роли": { en: "Costume for the role" },
  "Фото костюма": { en: "Costume photo" },
  Локации: { en: "Locations" },
  "Место сцены": { en: "Scene location" },
  "Опишите сцену, место или адрес места действия. Например: Бауманская, площадь Разгуляй. Не более 100 символов":
    {
      en: "Describe the scene, location or address of the location. For example: Baumanskaya, Razgulyay square. 100 characters max",
    },
  "Фото места": { en: "Photo of the location" },
  Продукт: { en: "Product" },
  "Назваание продукта": { en: "The product's name", ru: "Название продукта" },
  "Напишите название продукта. Например: Машина. Не более 100 символов": {
    en: "Write the name of the product. For example: Car. 100 characters max",
  },
  "Фото продукта": { en: "Product photo" },
  Реквизит: { en: "Props" },
  "Назваание рекзивита": { en: "Props name", ru: "Название рекзивита" },
  "Напишите название реквизита. Например: Кукла. Не более 50 символов": {
    en: "Write the name of the props. For example: Doll. 50 characters max",
  },
  "Фото реквизита": { en: "Photo of props" },
  Графика: { en: "Graphic arts" },
  "Назваание графики": { en: "Graphics title", ru: "Название графики" },
  "Напишите название графики. Например: стрит-арт. Не более 50 символов": {
    en: "Write the title of the graphic. For example: street art. 50 characters max",
  },
  "Фото графики": { en: "Photo of the art" },
  Музыка: { en: "Music" },
  "Назваание сингла": { en: "Single title", ru: "Название сингла" },
  "Напишите название сингла. Например: сон в летнюю ночь. Не более 100 символов":
    {
      en: "Write the title of the single. For example: a summer night's dream. 100 characters max",
    },
  "Ссылка на аудио-файл": { en: "Link to audio file" },
  "Добавьте ссылку на аудио-файл": { en: "Add a link to the audio file" },
  Тайминг: { en: "Timing" },
  Таймлайн: { en: "Timeline" },

  Что: { en: "What" },
  Почему: { en: "Why" },
  Как: { en: "How" },
  "Для кого": { en: "For whom" },
  Сколько: { en: "How much" },
  "С кем": { en: "With whom" },
  Референсы: { en: "References" },
  Кто: { en: "Who" },
  Контакты: { en: "Contacts" },
  "Название проекта": { en: "The project name" },
  "Напишите название проекта, например: Девятаев, Dead of night, Chat writer и т.д.Не более 100 символов.":
    {
      en: "Write the project name, e. g. Devyataev, Dead of night, Chat writer etc. 100 characters maximum",
    },
  Обложка: { en: "Cover" },
  "Тип проекта": { en: "Project type" },
  "Выберите из списка, или впишите свой вариант": {
    en: "Select from the list or write your own option",
  },
  Фильм: { en: "Film" },
  Сериал: { en: "TV show" },
  "Веб-сериал": { en: "Web-show" },
  "Анимационный проект": { en: "Animation project" },
  "Реалити-шоу": { en: "Reality show" },
  "Арт-проект": { en: "Art project" },
  Жанр: { en: "Genre" },
  "Укажите жанр своего проекта, не более 50 символов": {
    en: "Specify your project genre, 50 characters maximum",
  },
  Формат: { en: "Format" },
  "Укажите формат проекта, не более 50 символов": {
    en: "Specify the project format, 50 characters maximum",
  },
  "Краткое описание": { en: "Brief description" },
  "Опишите основную мысль своего проекта в 2-3 предложениях. Не более 150 символов.":
    {
      en: "Describe the main idea of your project in 2-3 sentences. 150 characters maximum.",
    },
  "Какую «боль» решает проект?": { en: "Why is this project important?" },
  "Расскажите какую боль решает ваш проект, какие цели преследует, почему он социально значим . Не более 500 символов.":
    {
      en: "Tell about the problem your project solves, goals pursued, social importance 500 characters maximum.",
    },
  "Какой ваш план по реализации проекта?": {
    en: "What is your plan regarding project implementation?",
  },
  "Опишите шаги, которые вы планируете предпринимать для реализации вашего проекта. Не более 500 символов.":
    {
      en: "Describe the steps you plan to take to implement your project. 500 characters maximum.",
    },
  "География вашей целевой аудитории": {
    en: "Geography of your target audience",
  },
  "Выберете из списка страны, где проживает аудитория, на которую направлен ваш проект":
    {
      en: "Select from the list the countries, where the audience for your project lives.",
    },
  Россия: { en: "Russia" },

  Австралия: {
    en: "Australia",
  },
  Австрия: {
    en: "Austria",
  },
  Азербайджан: {
    en: "Azerbaijan",
  },
  Албания: {
    en: "Albania",
  },
  Алжир: {
    en: "Algeria",
  },
  Ангола: {
    en: "Angola",
  },
  Андорра: {
    en: "Andorra",
  },
  "Антигуа и Барбуда": {
    en: "Antigua and Barbuda",
  },
  Аргентина: {
    en: "Argentina",
  },
  Армения: {
    en: "Armenia",
  },
  Афганистан: {
    en: "Afghanistan",
  },
  Багамы: {
    en: "Bahamas",
  },
  Бангладеш: {
    en: "Bangladesh",
  },
  Барбадос: {
    en: "Barbados",
  },
  Бахрейн: {
    en: "Bahrain",
  },
  Белоруссия: {
    en: "Belarus",
  },
  Белиз: {
    en: "Belize",
  },
  Бельгия: {
    en: "Belgium",
  },
  Бенин: {
    en: "Benin",
  },
  Болгария: {
    en: "Bulgaria",
  },
  Боливия: {
    en: "Bolivia",
  },
  "Босния и Герцеговина": {
    en: "Bosnia and Herzegovina",
  },
  Ботсвана: {
    en: "Botswana",
  },
  Бразилия: {
    en: "Brazil",
  },
  Бруней: {
    en: "Brunei",
  },
  "Буркина-Фасо": {
    en: "Burkina Faso",
  },
  Бурунди: {
    en: "Burundi",
  },
  Бутан: {
    en: "Butane",
  },
  Вануату: {
    en: "Vanuatu",
  },
  Великобритания: {
    en: "United Kingdom",
  },
  Венгрия: {
    en: "Hungary",
  },
  Венесуэла: {
    en: "Venezuela",
  },
  "Восточный Тимор": {
    en: "East Timor",
  },
  Вьетнам: {
    en: "Vietnam",
  },
  Габон: {
    en: "Gabon",
  },
  Гаити: {
    en: "Haiti",
  },
  Гайана: {
    en: "Guyana",
  },
  Гамбия: {
    en: "Gambia",
  },
  Гана: {
    en: "Ghana",
  },
  Гватемала: {
    en: "Guatemala",
  },
  Гвинея: {
    en: "Guinea",
  },
  "Гвинея-Бисау": {
    en: "Guinea-Bissau",
  },
  Германия: {
    en: "Germany",
  },
  Гондурас: {
    en: "Honduras",
  },
  Гренада: {
    en: "Grenada",
  },
  Греция: {
    en: "Greece",
  },
  Грузия: {
    en: "Georgia",
  },
  Дания: {
    en: "Denmark",
  },
  Джибути: {
    en: "Djibouti",
  },
  Доминика: {
    en: "Dominica",
  },
  "Доминиканская Республика": {
    en: "Dominican Republic",
  },
  Египет: {
    en: "Egypt",
  },
  Замбия: {
    en: "Zambia",
  },
  Зимбабве: {
    en: "Zimbabwe",
  },
  Израиль: {
    en: "Israel",
  },
  Индия: {
    en: "India",
  },
  Индонезия: {
    en: "Indonesia",
  },
  Иордания: {
    en: "Jordan",
  },
  Ирак: {
    en: "Iraq",
  },
  Иран: {
    en: "Iran",
  },
  Ирландия: {
    en: "Ireland",
  },
  Исландия: {
    en: "Iceland",
  },
  Испания: {
    en: "Spain",
  },
  Италия: {
    en: "Italy",
  },
  Йемен: {
    en: "Yemen",
  },
  "Кабо-Верде": {
    en: "Cape Verde",
  },
  Казахстан: {
    en: "Kazakhstan",
  },
  Камбоджа: {
    en: "Cambodia",
  },
  Камерун: {
    en: "Cameroon",
  },
  Канада: {
    en: "Canada",
  },
  Катар: {
    en: "Qatar",
  },
  Кения: {
    en: "Kenya",
  },
  Кипр: {
    en: "Cyprus",
  },
  Киргизия: {
    en: "Kyrgyzstan",
  },
  Кирибати: {
    en: "Kiribati",
  },
  Китай: {
    en: "China",
  },
  Колумбия: {
    en: "Colombia",
  },
  Коморы: {
    en: "Comoros",
  },
  Конго: {
    en: "Congo",
  },
  "ДР Конго": {
    en: "DR Congo",
  },
  КНДР: {
    en: "DPRK",
  },
  Корея: {
    en: "Korea",
  },
  "Коста-Рика": {
    en: "Costa Rica",
  },
  "Кот-д’Ивуар": {
    en: "Cote d'Ivoire",
  },
  Куба: {
    en: "Cuba",
  },
  Кувейт: {
    en: "Kuwait",
  },
  Лаос: {
    en: "Laos",
  },
  Латвия: {
    en: "Latvia",
  },
  Лесото: {
    en: "Lesotho",
  },
  Либерия: {
    en: "Liberia",
  },
  Ливан: {
    en: "Lebanon",
  },
  Ливия: {
    en: "Libya",
  },
  Литва: {
    en: "Lithuania",
  },
  Лихтенштейн: {
    en: "Liechtenstein",
  },
  Люксембург: {
    en: "Luxembourg",
  },
  Маврикий: {
    en: "Mauritius",
  },
  Мавритания: {
    en: "Mauritania",
  },
  Мадагаскар: {
    en: "Madagascar",
  },
  Малави: {
    en: "Malawi",
  },
  Малайзия: {
    en: "Malaysia",
  },
  Мали: {
    en: "Mali",
  },
  Мальдивы: {
    en: "Maldives",
  },
  Мальта: {
    en: "Malta",
  },
  Марокко: {
    en: "Morocco",
  },
  "Маршалловы Острова": {
    en: "Marshall Islands",
  },
  Мексика: {
    en: "Mexico",
  },
  Микронезия: {
    en: "Micronesia",
  },
  Мозамбик: {
    en: "Mozambique",
  },
  Молдавия: {
    en: "Moldova",
  },
  Монако: {
    en: "Monaco",
  },
  Монголия: {
    en: "Mongolia",
  },
  Мьянма: {
    en: "Myanmar",
  },
  Намибия: {
    en: "Namibia",
  },
  Науру: {
    en: "Nauru",
  },
  Непал: {
    en: "Nepal",
  },
  Нигер: {
    en: "Niger",
  },
  Нигерия: {
    en: "Nigeria",
  },
  Нидерланды: {
    en: "Netherlands",
  },
  Никарагуа: {
    en: "Nicaragua",
  },
  "Новая Зеландия": {
    en: "New Zealand",
  },
  Норвегия: {
    en: "Norway",
  },
  ОАЭ: {
    en: "UAE",
  },
  Оман: {
    en: "Oman",
  },
  Пакистан: {
    en: "Pakistan",
  },
  Палау: {
    en: "Palau",
  },
  Панама: {
    en: "Panama",
  },
  "Папуа — Новая Гвинея": {
    en: "Papua New Guinea",
  },
  Парагвай: {
    en: "Paraguay",
  },
  Перу: {
    en: "Peru",
  },
  Польша: {
    en: "Poland",
  },
  Португалия: {
    en: "Portugal",
  },
  Руанда: {
    en: "Rwanda",
  },
  Румыния: {
    en: "Romania",
  },
  Сальвадор: {
    en: "Salvador",
  },
  Самоа: {
    en: "Samoa",
  },
  "Сан-Марино": {
    en: "San Marino",
  },
  "Сан-Томе и Принсипи": {
    en: "Sao Tome and Principe",
  },
  "Саудовская Аравия": {
    en: "Saudi Arabia",
  },
  "Северная Македония": {
    en: "North Macedonia",
  },
  Сейшелы: {
    en: "Seychelles",
  },
  Сенегал: {
    en: "Senegal",
  },
  "Сент-Винсент и Гренадины": {
    en: "Saint Vincent and the Grenadines",
  },
  "Сент-Китс и Невис": {
    en: "Saint Kitts and Nevis",
  },
  "Сент-Люсия": {
    en: "Saint Lucia",
  },
  Сербия: {
    en: "Serbia",
  },
  Сингапур: {
    en: "Singapore",
  },
  Сирия: {
    en: "Syria",
  },
  Словакия: {
    en: "Slovakia",
  },
  Словения: {
    en: "Slovenia",
  },
  США: {
    en: "USA",
  },
  "Соломоновы Острова": {
    en: "Solomon islands",
  },
  Сомали: {
    en: "Somalia",
  },
  Судан: {
    en: "Sudan",
  },
  Суринам: {
    en: "Suriname",
  },
  "Сьерра-Леоне": {
    en: "Sierra Leone",
  },
  Таджикистан: {
    en: "Tajikistan",
  },
  Таиланд: {
    en: "Thailand",
  },
  Танзания: {
    en: "Tanzania",
  },
  Того: {
    en: "Togo",
  },
  Тонга: {
    en: "Tonga",
  },
  "Тринидад и Тобаго": {
    en: "Trinidad and Tobago",
  },
  Тувалу: {
    en: "Tuvalu",
  },
  Тунис: {
    en: "Tunisia",
  },
  Туркмения: {
    en: "Turkmenistan",
  },
  Турция: {
    en: "Turkey",
  },
  Уганда: {
    en: "Uganda",
  },
  Узбекистан: {
    en: "Uzbekistan",
  },
  Украина: {
    en: "Ukraine",
  },
  Уругвай: {
    en: "Uruguay",
  },
  Фиджи: {
    en: "Fiji",
  },
  Филиппины: {
    en: "Philippines",
  },
  Финляндия: {
    en: "Finland",
  },
  Франция: {
    en: "France",
  },
  Хорватия: {
    en: "Croatia",
  },
  ЦАР: {
    en: "CAR",
  },
  Чад: {
    en: "Chad",
  },
  Черногория: {
    en: "Montenegro",
  },
  Чехия: {
    en: "Czech",
  },
  Чили: {
    en: "Chile",
  },
  Швейцария: {
    en: "Switzerland",
  },
  Швеция: {
    en: "Sweden",
  },
  "Шри-Ланка": {
    en: "Sri Lanka",
  },
  Эквадор: {
    en: "Ecuador",
  },
  "Экваториальная Гвинея": {
    en: "Equatorial Guinea",
  },
  Эритрея: {
    en: "Eritrea",
  },
  Эсватини: {
    en: "Eswatini",
  },
  Эстония: {
    en: "Estonia",
  },
  Эфиопия: {
    en: "Ethiopia",
  },
  ЮАР: {
    en: "SOUTH AFRICA",
  },
  "Южный Судан": {
    en: "South Sudan",
  },
  Ямайка: {
    en: "Jamaica",
  },
  Япония: {
    en: "Japan",
  },

  США: { en: "USA" },
  Возраст: { en: "Аge" },
  "Выберете из списка возрастую категорию, на которую направлен ваш проект": {
    en: "Select from the list the age group for your project.",
  },
  "0-11 лет": { en: "0-11 years" },
  "12-18 лет": { en: "12-18 years" },
  "19-25 лет": { en: "19-25 years" },
  "26-35 лет": { en: "26-35 years" },
  "36-55 лет": { en: "36-55 years" },
  "56-70 лет": { en: "56-70 years" },
  "70+ лет": { en: "70+ years" },
  "Род деятельности": { en: "Line of activity" },
  "Характеристика аудитории": { en: "Line of activity" },
  "Укажите специфику интересов или рода деятельности вашей целевой аудитории. Например: мамы в декрете, офисные работники, блогеры и т.п. Не более 150 символов.":
    {
      en: "Tell about the interests or line of activity of your target audience. E. g.: mothers on maternity leave, office workers, bloggers etc. 150 characters maximum.",
    },
  Пол: { en: "Sex" },
  "Выберете из списка пол целевой аудитории": {
    en: "Select the target audience sex from the list",
  },
  "Не имеет значения": { en: "Irrelevant" },
  Мужчины: { en: "Male" },
  Женщины: { en: "Female" },
  "Бюджет проекта": { en: "Project implementation cost" },
  "Укажите сколько будет стоить реализация ващего проекта. Не более 50 символов. Оставьте пустым, если такой информации нет.":
    {
      en: "Specify the cost of your project implementation. 50 characters maximum. Leave blank if such information is unavailable.",
    },
  "Источник финансирования": { en: "Source of financing" },
  "Перечислите фактические и планируемые источники финансирования проекта. Не более 300 символов. Оставьте пустым, если такой информации нет.":
    {
      en: "List the actual and planned sources of the project financing. 300 characters maximum. Leave blank if such information is unavailable.",
    },
  "Какие основные этапы проекта?": {
    en: "What are the main stages of the project?",
  },
  "Название этапа": { en: "Stage name" },
  "Например, Дата выхода. Не более 100 символов": {
    en: "e. g. release date. 100 characters maximum",
  },
  "Дата этапа": { en: "Stage date" },
  Партнеры: { en: "Partners" },
  "Перечислите ключевых партнеров проекта": {
    en: "List the key project partners",
  },
  "Имя партнера": { en: "Partner's name" },
  "Компании, эксперты, менторы и т.д. Максимум 100 символов": {
    en: "companies, experts, mentors etc. 100 characters maximum.",
  },
  "Фото партнера": { en: "Partner's photo" },
  "Логотип компании или фото партнера": {
    en: "Company logo or partner's photo",
  },
  "Визитка вашего проекта": { en: "Card of your project" },
  "Добавьте ссылку на трейлер, интервью или др информацию о проекте. Оставьте пустым, если такой информации нет.":
    {
      en: "Add link to trailer, interview or other project information. Leave blank if such information is unavailable.",
    },
  "Введите поясняющий текст к референсу, если это необходимо. Не более 150 символов. Оставьте пустым, если такой информации нет.":
    {
      en: "Enter the descriptive text to the reference, if necessary 150 characters maximum. Leave blank if such information is unavailable.",
    },
  "Команда проекта": { en: "Project's team" },
  "Перечислите членов вашей команды": { en: "Name the members of your team" },
  Фото: { en: "Photo" },
  "Имя контактного лица": { en: "Name of the contact person" },
  "Укажите ФИО.": { en: "Full name" },
  Телефон: { en: "Telephone" },
  "Введите контактный номер телефона": { en: "Enter the contact phone number" },
  Почта: { en: "E-mail" },
  "Укажите контактную почту": { en: "Enter the contact e-mail" },
  Картинка: { en: "Picture" },

  "Название УТП": {},
  "Напишите название вашего уникального торгового предложения (это может быть название, слоган и т.д.). Например: Данон, все лучшее в йогурте. Не более 100 символов":
    {},
  "Загрузите свою картинку или воспользуйтесь картиной из стоковой галереи. Это может быть логотитип, символ или что-то, с чем у потребителя может возникнуть ассоциация с вашим предложением":
    {},
  "Информация о компании": {},
  "Напишите краткую информацию о вашей компании. Не более 200 символов.": {},
  Показатели: {},
  "Укажите показатели компании или продукта. Например (количество клиентов/пользователей, доля рынка и т.д.)":
    {},
  "Загрузите свою картинку или воспользуйтесь картиной из стоковой галереи. Это может быть графическое отображение показателей, диаграмма, график и т.д.":
    {},
  "Какую проблему решает УТП": {},
  "Коротко опишите, какую пользу несет УТП. Например: Приложение APS позволяет экономить время на всех ручных процессах. Не более 200 символов":
    {},
  Тезисы: {},
  "Тезисно сформулируйте положительные стороны УТП. Например: эффективность повышвется на 10%. Не более 200 символов":
    {},
  "Преимущества сотрудничества": {},
  "Список тезисов с преимуществами": {},
  "Тезисно сформулируйте преимущества сотрудничества. Например: лучшая служба поддержки. Не более 200 символов":
    {},
  "Успешное внедрение": {},
  "Кейсы успешного внедрения": {},
  "Опишите кейсы с успешным внедрением вашего продукта. Не более 400 символов":
    {},
  "Уникальность предложения": {},
  "Ключевые отличия": {},
  "Введите краткую информацию о ключевых отличиях вашего продукта. Не более 400 символов":
    {},
  "Тезисно сформулируйте ключевые отличия. Например: единственные в России предоставляем услугу сопровождения клиринга. Не более 400 символов":
    {},
  Сотрудничество: {},
  "Условия сотрудничества": {},
  "Тезисно описаните условия сотрудничества. Например: заключение сделки за 2 дня, полата по результату. Не более 400 символов":
    {},
  "Подтверждение качества": {},
  "Качество и сертификация": {},
  "Введите краткую информацию о вашей сертификации и качестве. Например: компания каждый год проводит переаттестацию специалистов и проделевает лицензию на авторские права. Не более 400 символов":
    {},
  "Загрузите сертификат, грамоту, лицензию": {},
  "Загрузите свою картинку или воспользуйтесь картиной из стоковой галереи. Это может быть сертификат, грамота, лицензия и т.д.":
    {},
  "Пресса и СМИ о компании": {},
  "Приведенные выдержки из СМИ и прессы о вашей компании. Не более 400 символов":
    {},
  "Загрузите свою картинку или воспользуйтесь картиной из стоковой галереи. Это может быть скрин с сайта или документ":
    {},
  "СТА и контакты": {},
  "Укажите контакты и способ связи. Например: для рекамы пишите market@mail.ru, по продажам звоните +795500000. Не более 200 символов":
    {},

  "Название стартапа/продукта": {},
  "Целевая аудитория": {},
  "Описание целевой аудитории": {},
  "Опишите вашу целевую аудиторию, на которую направлен продукт. Не более 400 символов":
    {},
  "Проблемы пользователей": {},
  "Описание проблем пользователей": {},
  "Опишите проблемы, которые возникают у пользователей. Не более 400 символов":
    {},
  "Альтернативы решения проблем пользователей": {},
  "Альтернатвные решения проблем пользователей": {},
  "Напишите перечень существующих решений, которые решают или могут решить проблемы пользователей. Максимум 600 символов":
    {},
  "Предлагаемое решение": {},
  "Напишите предполагаемое решение проблем пользователей. Максимум 600 символов":
    {},
  "Уникальность решения": {},
  "Опишите уникальность предлагаемого решения, основные отличая от существующих решений. Максимум 600 символов":
    {},
  "Источники доходов": {},
  "Тезисно сформулируйте источники доходов вашего проекта. Максимум 600 символов":
    {},
  "Каналы продаж": {},
  "Опишите, как клиенты узнают о продукте. Максимум 600 символов": {},
  Метрики: {},
  "Ключевые метрики проекта": {},
  "Тезисное сформулируйте критерии успеха проекта, перечень измеримых показателей. Максимум 600 символов":
    {},
  "Структура затрат": {},
  "Первоначальные затраты на создание": {},
  "Тезисное сформулируйте, какие затраты планируются на создание проекта. Максимум 600 символов":
    {},
  "Затраты на развитие": {},
  "Тезисное сформулируйте, какие затраты планируются на развитие проекта. Максимум 600 символов":
    {},
  "Скрытые преимущества": {},
  "Опишите, что не позволит конкурентам быстро повторить продукт. Максимум 600 символов":
    {},

  "Что?": { en: "What?" },
  "Почему?": { en: "Why?" },
  "Как?": { en: "How?" },
  "Для кого?": { en: "For whom?" },
  "Сколько?": { en: "How much?" },
  "С кем?": { en: "With whom?" },
  "На что похоже?": { en: "What does it look like?" },
  "Кто?": { en: "Who?" },
  "Когда?": { en: "When?" },
  "Как связаться?": { en: "How to get in touch?" },
  "Шаблон контента": { en: "Content template" },
  "Уникальное торговое предложение": { en: "Unique selling proposition" },
  "Перечислите основные этапы проекта с указанием даты начала этапа": {
    en: "List the milestones of the project with the start date of the stage",
  },
  "Укажите наиболее похожие продукты, сервисы": {
    en: "Indicate the most similar projects or companies",
  },
  Ссылка: { en: "Link" },
  "Добавьте ссылку на сайт, трейлер, интервью или др информацию о проекте. Оставьте пустым, если такой информации нет.":
    {
      en: "Add a link to the website, trailer, interview or other information about the project. Leave blank if there is no such information.",
    },
  "Форма партнертсва": { en: "Partnership form" },
  Сценарист: { en: "Screenwriter" },
  Режиссер: { en: "Director" },
  Продюсер: { en: "Producer" },
  Оператор: { en: "Operator" },
  Художник: { en: "Artist" },
  Композитор: { en: "Composer" },
  Актер: { en: "Actor" },
  Супервайзер: { en: "Supervisor" },
  Валюта: { en: "Currency" },
  "Выберете валюту из списка": { en: "Choose a currency from the list" },
  Инвестор: { en: "Investor" },
  Спонсор: { en: "Sponsor" },
  "Продакт плейсмент": { en: "Product placement" },
  "Презентация продуктa": { en: "Product presentation" },
  Бизнес: { en: "Business" },
  Реклама: { en: "Advertising" },
  Контент: { en: "Content" },
  Игра: { en: "Game" },
  "IT-проект": { en: "IT-project" },
  Игра: { en: "Game" },
  Шоу: { en: "Show" },
  "Весь мир": { en: "The whole world" },
  Подрядчик: { en: "Contractor" },
  Cоучредитель: { en: "Co-founder" },
  Cопродюсер: { en: "Co-producer" },
};
