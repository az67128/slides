import {writable, derived} from 'svelte/store';
import slidesTranslation from './slidesTranslation'
export const FALLBACK_LANG = "ru";

const detectLang = () => {
  if (localStorage.getItem("slides-lang"))
    return localStorage.getItem("slides-lang");
  if (window.navigator.languages)
    return window.navigator.languages.some(
      (item) => item === "ru" || item === "ru-RU"
    )
      ? "ru"
      : FALLBACK_LANG;
  const language = window.navigator.userLanguage || window.navigator.language;
  return language === "ru" || language === "ru-RU" ? "ru" : FALLBACK_LANG;
};

export const lang = writable(detectLang());
lang.subscribe(state=>localStorage.setItem("slides-lang", state))


export const translation=(dict) => {
    return derived(lang, $lang => (phrase)=> dict[phrase] ? dict[phrase][$lang] || phrase : phrase);
}

export const slidesText = translation(slidesTranslation)