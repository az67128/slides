#!/usr/bin/env bash

echo "Как назовем новый проект? уже есть названия: "
ls -m ./src/templatesBuilder/templates
echo

printf "Template name: "
read TEMPLATE_NAME
# #TODO - check exist, alert if exist

cp -r ./src/templatesBuilder/_templatePattern ./src/templatesBuilder/templates/${TEMPLATE_NAME}
cd ./src/templatesBuilder/templates/${TEMPLATE_NAME}

npm run dev
