import svelte from "rollup-plugin-svelte";
import commonjs from "@rollup/plugin-commonjs";
import resolve from "@rollup/plugin-node-resolve";
import livereload from "rollup-plugin-livereload";
import { terser } from "rollup-plugin-terser";
import css from "rollup-plugin-css-only";
import fs from "fs";
import path from "path";
import alias from '@rollup/plugin-alias';

const production = !process.env.ROLLUP_WATCH;

function inlineSvelte(template, dest) {
  return {
    name: "Svelte Inliner",
    generateBundle(opts, bundle) {
      const file = path.parse(opts.file).base;
      const code = bundle[file].code;
      const css = bundle["bundle.css"].source;

      const output = fs.readFileSync(template, "utf-8");

      bundle[file].code = output
        .replace("%%script%%", () => code)
        .replace("%%css%%", () => css);
    },
  };
}

function serve() {
  let server;

  function toExit() {
    if (server) server.kill(0);
  }

  return {
    writeBundle() {
      if (server) return;
      server = require("child_process").spawn(
        "npm",
        ["run", "start", "--", "--dev"],
        {
          stdio: ["ignore", "inherit", "inherit"],
          shell: true,
        }
      );

      process.on("SIGTERM", toExit);
      process.on("exit", toExit);
    },
  };
}

export default {
  input: "src/main.js",
  output: {
    sourcemap: false,
    format: "iife",
    name: "app",
    file: production ? "public/build/bundle.html"  : "public/build/bundle.js",
  },
  plugins: [
    svelte({
      dev: !production, // run-time checks
      css: (css) => css.write(`bundle.css`),
    }),
    // we'll extract any component CSS out into
    // a separate file - better for performance
    css({ output: "bundle.css" }),

    // If you have external dependencies installed from
    // npm, you'll most likely need these plugins. In
    // some cases you'll need additional configuration -
    // consult the documentation for details:
    // https://github.com/rollup/plugins/tree/master/packages/commonjs
    resolve({
      browser: true,
      dedupe: ["svelte"],
    }),
    commonjs(),

    // In dev mode, call `npm run start` once
    // the bundle has been generated
    !production && serve(),

    // Watch the `public` directory and refresh the
    // browser on changes when not in production
    !production && livereload("public"),

    // If we're building for production (npm run build
    // instead of npm run dev), minify
    production && terser(),
    production && inlineSvelte("src/template.html"),
    alias({
      resolve: [".svelte", ".js"],
      entries: [
          { find: "common", replacement: path.resolve(__dirname, "../../_common") },
      ]
    }),
  ],
  watch: {
    clearScreen: false,
  },
};
