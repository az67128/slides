export default {
    "car": {
        "index": 0,
        "label": "Есть",
        "value": "Есть"
    },
    "fee": "100000",
    "job": "atol",
    "city": "Москва",
    "role": "Актерское амплуа",
    "cover": "https://teasy.one/media/user_pics/photo-1535016120720-40c646be5580.jpeg",
    "email": "az67128@gmail.com",
    "phone": "+79268855308",
    "price": "10000",
    "visas": [
        {
            "id": "HRNCus6KdTbqDLpx-WUCQ",
            "country": "Россия",
            "issueTo": "10.08.2021"
        },
        {
            "id": "fRE8hYMYddRtC6_L25tKH",
            "country": "Уганда",
            "issueTo": "10.08.2021"
        }
    ],
    "genres": [
        {
            "index": 3,
            "label": "участие в конкурсах",
            "value": "участие в конкурсах"
        },
        {
            "index": 5,
            "label": "реалити-шоу",
            "value": "реалити-шоу"
        },
        {
            "index": 6,
            "label": "спектакли",
            "value": "спектакли"
        },
        {
            "index": 11,
            "label": "Свой вариант",
            "value": "свой вариант жанра"
        }
    ],
    "height": "158",
    "social": [
        {
            "id": "g_2vzHKBVLu8mRQK3iwHt",
            "link": "http://localhost:5000/slide/9105cca419_khgbhmlo/edit"
        },
        {
            "id": "nJnSsEh6IqCOONsHWJdyz",
            "link": "http://localhost:5000/slide/9105cca419_khgbhmlo/edit"
        }
    ],
    "styles": [
        {
            "index": 0,
            "label": "импровизация",
            "value": "импровизация"
        },
        {
            "index": 2,
            "label": "пародия",
            "value": "пародия"
        }
    ],
    "weight": "234",
    "carType": "suzuki",
    "courses": "Пройденные курсы\nПройденные курсы\nПройденные курсы",
    "marriga": {
        "index": 0,
        "label": "Холост / Не замужен",
        "value": "Холост / Не замужен"
    },
    "pension": {
        "file": "https://teasy.one/media/user_docs/sample_nQzYlsT.pdf",
        "fileType": "URL"
    },
    "children": [
        {
            "id": "LMvXE2XQCQTsthm-RmTf3",
            "age": "15",
            "name": "Ребенок1"
        },
        {
            "id": "PId9-_4igQGm85F79TN6l",
            "age": "23",
            "name": "Александр Зинченко"
        }
    ],
    "fullname": "Фамилия Имя Отчество",
    "legsSize": "45",
    "lookType": "нордический",
    "passport": {
        "file": "https://teasy.one/media/user_docs/sample_nQzYlsT.pdf",
        "fileType": "FILE"
    },
    "portrait": "https://teasy.one/media/user_pics/photo-1613614172336-6bc4a8b9586d.jpeg",
    "showRill": "http://localhost:5000/slide/9105cca419_khgbhmlo/edit",
    "agnetName": "Имя агента",
    "agreement": {
        "index": 0,
        "label": "Да",
        "value": "Да"
    },
    "birthDate": "01.08.2000",
    "education": "МГАУ ИМ ПВ Образование, укажите ВУЗ и факультет",
    "eyesColor": "карие",
    "hairColor": "рыжие",
    "languages": [
        {
            "index": 0,
            "label": "русский",
            "value": "русский"
        },
        {
            "index": 1,
            "label": "английский",
            "value": "английский"
        }
    ],
    "photoFull": "https://teasy.one/media/user_pics/photo-1606787366850-de6330128bfc.jpeg",
    "agnetEmail": "az67128@gmail.com",
    "agnetPhone": "+79268855308",
    "closesSize": "xl",
    "references": "интернеты",
    "projectName": "casting",
    "carAgreement": {
        "index": 0,
        "label": "Да",
        "value": "Да"
    },
    "templateLang": "ru",
    "drivingLicense": {
        "index": 0,
        "label": "Есть",
        "value": "Есть"
    },
    "videoVisitCard": "http://localhost:5000/slide/9105cca419_khgbhmlo/edit",
    "foreignPassport": {
        "file": "https://teasy.one/media/user_docs/sample_nQzYlsT.pdf",
        "name": "sample.pdf",
        "fileType": "FILE"
    },
    "drivingLicenseAge": "12",
    "drivingLicenseCategory": "B"
}