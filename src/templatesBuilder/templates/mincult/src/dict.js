export default {
    what: { ru: 'О чем?', en: 'What?' },
    genre: { ru: 'Жанр', en: 'Genre' },
    format: { ru: 'Формат', en: 'Format' },
    importance: {
        ru: 'Почему?',
        en: 'Why is this project important?',
    },
    concept: {ru: 'Концепция', en: 'Concept' },
    synopsis: {ru: 'Синопсис', en: 'Synopsis' },
    explication: {ru: 'Режиссерская экспликация', en: 'Director\'s explication'},
    plan: {
        ru: 'Как?',
        en: 'Project implementation plan',
    },
    forWho: {
        ru: 'Для кого?',
        en: 'Who is the project for?',
    },
    script: {ru:'Сценарий', en: 'Screenplay'},
    geography: { ru: 'География', en: 'Geography' },
    age: { ru: 'Возраст', en: 'Age' },
    job: {
        ru: 'Род деятельности',
        en: 'Line of activity',
    },
    sex: { ru: 'Пол', en: 'Gender' },
    cost: {
        ru: 'Сколько?',
        en: 'HOW MUCH IS THE PROJECT?',
    },
    price: {
        ru: 'Стоимость реализации проекта',
        en: 'Project implementation cost',
    },
    source: {
        ru: 'Источник финансирования',
        en: 'Source of financing',
    },
    when: {
        ru: 'Когда?',
        en: 'Duration of the project',
    },
    partners: { ru: 'С кем?', en: 'Partners' },
    reference: { ru: 'Референсы', en: 'Reference' },
    filmography: {ru: 'Фильмографии', en: 'Filmography'},
    legacy: {ru: 'Предыдущие работы', en: 'Previous work'},
    team: { ru: 'Кто?', en: 'WHO MAKES THE PROJECT?' },
    letter: {
        ru: 'Письмо от художественного руководителя',
        en: 'Letter from the artistic director'
    },
    contacts: { ru: 'Контакты', en: 'Contacts' },
};
