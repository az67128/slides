export default {
    "age": [
        {
            "index": 3,
            "label": "26-35 лет",
            "value": "26-35 лет"
        },
        {
            "index": 2,
            "label": "19-25 лет",
            "value": "19-25 лет"
        }
    ],
    "job": "Те кто ебошит",
    "sex": {
        "index": 1,
        "label": "Мужчины",
        "value": "Мужчины"
    },
    "plan": "Берем и ебашим, очевидно же",
    "team": [
        {
            "id": "kdVgIztGY-s-EfFvbpSSP",
            "name": "Первый имя",
            "image": "https://teasy.one/media/user_pics/Rectangle_8_J826RPu.png",
            "position": {
                "index": 0,
                "label": "Сценарист",
                "value": "Сценарист"
            }
        },
        {
            "id": "IC4hRkfpC-ZyKcZaE8BI6",
            "name": "Второй имя",
            "image": "https://teasy.one/media/user_pics/actor1.png",
            "position": {
                "index": 4,
                "label": "Художник",
                "value": "Художник"
            }
        },
        {
            "id": "r7ARGEhR5tGU6KcoGYfh3",
            "name": "aaaaaa",
            "image": "https://teasy.one/media/user_pics/Rectangle_6_LRVQoeX.png"
        }
    ],
    "when": [
        {
            "id": "q7IGiQ-20GfoX4Yf7JaNn",
            "stageDate": "09.03.2021",
            "stageName": "Первый этап"
        },
        {
            "id": "-TklECvMOOEmD5vOpGAeB",
            "stageDate": "24.08.2021",
            "stageName": "Второй этап"
        }
    ],
    "cover": "https://teasy.one/media/user_pics/mincult_qhZvALb.jpg",
    "genre": "Название жанра",
    "price": "1 доллар и пара шекелей",
    "format": "Название формата",
    "legacy": [
        {
            "id": "UxjsmBccpuAKeWmA3CYOq",
            "file": {
                "file": "https://teasy.one/media/user_docs/%D0%BF%D0%BF_%D0%A2%D0%90%D0%98%D0%9C%D0%9F%D0%AD%D0%94.pdf",
                "name": "Пример того что сделано",
                "fileType": "FILE"
            }
        },
        {
            "id": "Cwygr1qJWT7ulqLMI0qI3",
            "file": {
                "file": "https://teasy.one/media/user_docs/%D0%BF%D0%BF_%D0%A2%D0%90%D0%98%D0%9C%D0%9F%D0%AD%D0%94.pdf",
                "name": "еще один пример",
                "fileType": "FILE"
            }
        }
    ],
    "letter": "Текст письма от художественного руководителя",
    "script": {
        "file": "https://teasy.one/media/user_docs/%D0%9A%D0%BE%D0%B4%D0%B5%D0%BA%D1%81_%D0%BF%D0%BE%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D1%8F.pdf",
        "name": "Ссылка на сценарий",
        "fileType": "FILE"
    },
    "concept": "текст концепции",
    "finance": "То что соберем, то и будет.",
    "currency": {
        "index": 0,
        "label": "$",
        "value": "$"
    },
    "document": {
        "file": "https://teasy.one/media/user_docs/%D0%9A%D0%BE%D0%B4%D0%B5%D0%BA%D1%81_%D0%BF%D0%BE%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D1%8F.pdf",
        "name": "Кодекс_поведения",
        "fileType": "FILE"
    },
    "partners": [
        {
            "id": "ObxWneHbpVXUI-k017CW-",
            "name": "Первый партнер",
            "image": "https://teasy.one/media/user_pics/vovka-v-tridevyatom-tsarstve.jpg",
            "partnershipForm": {
                "index": 1,
                "label": "Спонсор",
                "value": "Спонсор"
            }
        },
        {
            "id": "hAkUISHqR_8hbu699KY8d",
            "name": "Второй партнер",
            "image": "https://teasy.one/media/user_pics/pngtree-power-pop-art-woman-hand-drawn-vintage-illustration-element-png-image_2157260.jpg",
            "partnershipForm": {
                "index": 0,
                "label": "Инвестор",
                "value": "Инвестор"
            }
        },
        {
            "id": "4JaZARvAH5t--5uvCI4jS",
            "name": "Третий партнер",
            "image": "https://teasy.one/media/user_pics/Rectangle_3_3_fqCY7a9.png"
        }
    ],
    "synopsis": "текст синопсиса",
    "geography": [
        {
            "index": 1,
            "label": "Россия",
            "value": "Россия"
        },
        {
            "index": 2,
            "label": "Украина",
            "value": "Украина"
        },
        {
            "index": 3,
            "label": "Беларусь",
            "value": "Беларусь"
        }
    ],
    "reference": [
        {
            "id": "XTiD1k_U_ahOqXNee7N8Z",
            "link": "https://google.com",
            "name": "Гугл",
            "referenceDescription": "Типа крутой поисковик"
        },
        {
            "id": "6UMxAYPWkzLoeEPQUEKv5",
            "link": "https://ya.ru",
            "name": "Ядекс",
            "referenceDescription": "Минималистичный дезигн"
        }
    ],
    "importance": "Просто большую попаболь, того что нужно наполнить шаблон контентом",
    "contactMail": "Ул Пушкина дом Колотушкина",
    "contactName": "Имя контактного лица ",
    "description": "Краткое описание  \nможет быть большим",
    "explication": "текст экспликации",
    "filmography": [
        {
            "id": "WsT06ui-usoMNlT2xcLxh",
            "file": {
                "fileType": "URL"
            },
            "team": [
                {
                    "id": "8-MVDifGo2HAG0tQPWFs5",
                    "name": "Первый чувак",
                    "position": {
                        "index": 0,
                        "label": "Сценарист",
                        "value": "Сценарист"
                    }
                },
                {
                    "id": "A97OYr3gs2KKBqUywVHzi",
                    "name": "Второй чувак",
                    "position": {
                        "index": 3,
                        "label": "Оператор",
                        "value": "Оператор"
                    }
                }
            ],
            "image": "https://teasy.one/media/user_pics/Frame_50.png",
            "title": "Название 1го фильма"
        },
        {
            "id": "G0rwuTSGo9udpSHJPfh7T",
            "team": [
                {
                    "id": "J-Txb7ywdWzT4GEBtEHdY",
                    "name": "Вовка",
                    "position": {
                        "index": 4,
                        "label": "Художник",
                        "value": "Художник"
                    }
                }
            ],
            "image": "https://teasy.one/media/user_pics/vovka-v-tridevyatom-tsarstve.jpg",
            "title": "Еще один фильм"
        }
    ],
    "projectName": "Название проекта (Подача в минкульт)",
    "projectType": {
        "index": 2,
        "label": "Веб-сериал",
        "value": "Веб-сериал"
    },
    "contactImage": "https://teasy.one/media/user_pics/Frame_220.png",
    "contactPhone": "9 09090 9090 000 0",
    "templateLang": "ru"
}
