export default {
  name: "Кто запрашивает участие?",
  time: "1 час",
  cover:
    "https://teasy.one/media/user_pics/photo-1624625848211-0dc5604f0deb.jpeg",
  format: { index: 0, label: "он-лайн", value: "он-лайн" },
  target:
    "Цель организатора Цель организатораЦель организатораЦель организатораvЦель организатораЦель организатораЦель организатора. Цель организатораЦель организатораЦель организатора",
  company: "Организатор мероприятия",
  endDate: "31.08.2021",
  meeting: [
    {
      id: "Au5USKcLrzsk-O40-Bd1R",
      meetingMail: "Электронная почта",
      meetingName: "Имя",
      meetingPhone: "Телефон",
    },
    {
      id: "p1tZfx_bRyRXZ31ilEYdl",
      meetingMail: "Электронная почта2",
      meetingName: "имя2",
      meetingPhone: "Телефон2",
    },
  ],
  audience: "Аудитория Аудитория Аудитория Аудитория",
  eventLink: "link",
  materials:
    "Какие материалы необходимо подготовить?Какие материалы необходимо подготовить?",
  ourTarget:
    "Цель участникаЦель участникаЦель участникаЦель участникаЦель участникаЦель участникаЦель участникаЦель участника. Цель участникаЦель участникаЦель участникаЦель участника",
  startDate: "26.08.2021",
  startTime: "12.00",
  streaming: "Где и каким образом будет транслироваться мероприятие?",
  eventPlace: "где-то",
  eventTheme: "Тема мероприятия",
  pitchTheme: "Тема выступления",
  contactName: "Имя контактного лица ",
  projectName: "Название мероприятия",
  requirement: "фываыва",
  responsible: [
    {
      id: "xr5PP1u7jR2hmVuNxHILU",
      responsibleMail: "почта",
      responsibleName: "Имя ",
      responsiblePhone: "телефон",
    },
  ],
  contactPhone: "Телефон контактного лица",
  contactemail: "Электронная почта контактного лица",
  templateLang: "ru",
  requestedPrice: "123123",
  suggestedPrice: "10000",
};
