export default {
  time: "30 минут",
  cover: "https://images.pexels.com/photos/845451/pexels-photo-845451.jpeg",
  ideas: [
    {
      id: "PtvA7BEa3uxWGuV1yuqvg",
      idea: "Идеи и новые вопросы, возникшие на звонке",
    },
    {
      id: "VKOFn4YSZKf49zhIG4Xx2",
      idea: "Идеи и новые вопросы, возникшие на звонке",
    },
    {
      id: "B9qnuFYzKdwfy0hvDnTU5",
      idea: "Идеи и новые вопросы, возникшие на звонке",
    },
    { id: "MrGkjCCSqqpsMSoD134Ho" },
  ],
  tasks: [
    {
      id: "ccw9aX2chvAZK0rQiuz2p",
      task: "Задача",
      duedate: "11.02.2022",
      responsilbe: "Ответственный",
    },
    {
      id: "zStfgBn3imTMw-bC92ekh",
      task: "Задача",
      duedate: "22.01.2022",
      responsilbe: "Ответственный",
    },
    { id: "_C1YUrmYjUFaY9Rp-OrRj", duedate: "22.01.2022" },
    { id: "0UKg-ReOlJMsF7Q1NxuoJ", task: "Задача", duedate: "22.01.2022" },
  ],
  agenda: [
    {
      id: "OT4DneaIsvkEzgINyRofe",
      question: "Вопрос",
      solution: "Предлагаемое решение",
    },
    {
      id: "WsfIh5HXjeeck1gKK1JYN",
      question: "Вопрос",
      solution: "Предлагаемое решение",
    },
    {
      id: "rpoJkVTbI_EteDqchhyWk",
      question: "Вопрос",
      solution: "Предлагаемое решение",
    },
    {
      id: "eIbkVzTVv-WxoPxDpb2lU",
      question: "Вопрос",
      solution: "Предлагаемое решение",
    },
  ],
  result: [
    {
      id: "gp-5q8MHBbuZqJU8F1o4L",
      solution: "Принятые решения по каждому вопросу",
    },
    {
      id: "pHsF-yzsfpsLI4e6BGq-x",
      solution: "Принятые решения по каждому вопросу",
    },
    {
      id: "A3JyYHCj3Sqqlq98QXofU",
      solution: "Принятые решения по каждому вопросу",
    },
    {
      id: "fEXtppTBz_mYdM_8ADm5U",
      solution: "Принятые решения по каждому вопросу",
    },
    {
      id: "Yj0fq5w8BqFkTWahDjEiS",
      solution: "Принятые решения по каждому вопросу",
    },
  ],
  comments: [
    {
      id: "6ZGpV1wNG0mzPAl7VHgX6",
      comment: "Нерешенные вопросы и комментарии",
    },
    {
      id: "TF6aPEzfIfhufj_QGflfO",
      comment: "Нерешенные вопросы и комментарии",
    },
    {
      id: "pJ51MimYzV70BgxDBROZk",
      comment: "Нерешенные вопросы и комментарии",
    },
    { id: "qkdJMkzM0Tmi3IzRZUvDJ" },
  ],
  materials: [
    {
      id: "SZC5K8RNPJm47GPQM98gn",
      document: {
        file: "https://teasy.one/slide/kkarena_simkvadx",
        name: "https://teasy.one/slide/kkarena_simkvadx",
        fileType: "URL",
      },
    },
    {
      id: "1DGh0crl2YLRAl98IOQqE",
      document: {
        file: "https://teasy.one/slide/kkarena_simkvadx",
        name: "https://teasy.one/slide/kkarena_simkvadx",
        fileType: "URL",
      },
    },
  ],
  requester: "Кто запрашивает звонок?",
  projectName: "Тема звонка или встречи",
  participants: [
    { id: "vG5Bn0zYTo1df2lmwerJB", name: "Фамилия1" },
    { id: "mPRRCoGs3b5F5SnRlEZSH", name: "Фамилия2" },
    { id: "ZFH0JVJR68_hgB1o8pkUV", name: "Фамилия3" },
  ],
  templateLang: "ru",
  timeproposal: "Пожелания к дате и времени звонка",
};
