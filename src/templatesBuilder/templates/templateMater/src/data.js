export default {
  name: "Александр Зинченко",
  email: "az67128@gmail.com",
  phone: "+79268855308",
  blocks: [
    {
      id: "kXC-myaWa0LfbO5zZPezB",
      fields: [
        {
          id: "5wr1HxRA0RenGPtxIgcx9",
          type: { index: 2, label: "изображение", value: "изображение" },
          comments: "Комментарий",
          fieldName: "Поле 1",
          fieldDescription: "Подсказка 1",
        },
        {
          id: "IL_UZRmxmWYGmhkZv2ag3",
          type: { index: 1, label: "длинный текст", value: "длинный текст" },
          comments: "Комментарий",
          fieldName: "Поле 2",
          fieldDescription: "Подсказка 2",
        },
        {
          id: "EsY_-b9cG6DGKxjHXOq-b",
          type: { index: 4, label: "документ", value: "документ" },
          comments: "Комментарий",
          fieldName: "Поле 3",
          fieldDescription: "Подсказка 3",
        },
      ],
      blockName: "Блок 1",
    },
    {
      id: "Hp1HOs0HoLUvRvmrJkE2p",
      fields: [
        {
          id: "-Q8bHTLA40eV3KaeJxh4E",
          type: { index: 3, label: "опция", value: "опция" },
          comments: "ололо",
          fieldName: "поле 1",
          fieldDescription: "подсказка 1",
        },
      ],
      blockName: "Блок 2",
    },
  ],
  company: "Атол",
  category: { index: 1, label: "бизнес", value: "бизнес" },
  telegram: "@az67128",
  projectName: "Шаблон шаблонович",
};
