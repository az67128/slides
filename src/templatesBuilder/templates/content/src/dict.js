export default {
    what: { ru: 'О чем?', en: 'What?' },
    genre: { ru: 'Жанр', en: 'Genre' },
    format: { ru: 'Формат', en: 'Format' },
    importance: {
        ru: 'Почему?',
        en: 'Why is this project important?',
    },
    plan: {
        ru: 'Как?',
        en: 'Project implementation plan',
    },
    forWho: {
        ru: 'Для кого?',
        en: 'Who is the project for?',
    },
    geography: { ru: 'География', en: 'Geography' },
    age: { ru: 'Возраст', en: 'Age' },
    job: {
        ru: 'Характеристика аудитории',
        en: 'Line of activity',
    },
    sex: { ru: 'Пол', en: 'Gender' },
    cost: {
        ru: 'Сколько?',
        en: 'HOW MUCH IS THE PROJECT?',
    },
    price: {
        ru: 'Стоимость реализации проекта',
        en: 'Project implementation cost',
    },
    source: {
        ru: 'Источник финансирования',
        en: 'Source of financing',
    },
    when: {
        ru: 'Когда?',
        en: 'Duration of the project',
    },
    partners: { ru: 'С кем?', en: 'Partners' },
    reference: { ru: 'Референсы', en: 'Reference' },

    team: { ru: 'Кто?', en: 'WHO MAKES THE PROJECT?' },
    contacts: { ru: 'Контакты', en: 'Contacts' },
};
