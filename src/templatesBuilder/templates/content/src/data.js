export default {
  age: [{ index: 7, label: "Свой вариант", value: "14-20" }],
  job: "школьники и студенты",
  sex: { index: 0, label: "Все", value: "Все" },
  plan: "Студия Скринлайф привлекает 10 независимых дебютных команд, предоставляет им технологическую и методологическую поддержку, но дает свободу творчества. ",
  team: [
    {
      id: "nOhVKEe-emxsTqaE28Xai",
      name: "Марина Хренкова",
      position: {
        index: 8,
        label: "Свой вариант",
        value: "Head of Production",
      },
    },
    {
      id: "k6btE7dC1O-I7EQoRjhWE",
      name: "Илья Мелешкин",
      position: {
        index: 8,
        label: "Свой вариант",
        value: "Head of Post Production",
      },
    },
    {
      id: "ThkBwWne30YBpBOPGGkb3",
      name: "Илья Зевакин",
      position: { index: 8, label: "Свой вариант", value: "Head of Editing" },
    },
  ],
  when: [
    { id: "1", stageDate: "11.01.2022", stageName: "Запуск проекта" },
    { id: "2", stageDate: "01.03.2022", stageName: "Начало производства" },
    { id: "3", stageDate: "31.10.2022", stageName: "Окончание производства" },
    { id: "4", stageDate: "01.10.2022", stageName: "Выход" },
  ],
  price: "200млн",
  currency: { index: 2, label: "₽", value: "₽" },
  document: { fileType: "URL" },
  partners: [
    {
      id: "mu_TOSQWcVvnro0Y0nQIN",
      name: "МХАТ",
      partnershipForm: {
        index: 3,
        label: "Свой вариант",
        value: "студенты 4ого курса будут главными актерами проект",
      },
    },
    {
      id: "9fquUYZahsaOVBP76GN-C",
      name: "ВШЭ",
      partnershipForm: {
        index: 3,
        label: "Свой вариант",
        value: "помощь с образовательной программой ",
      },
    },
    {
      id: "gOYpwLqJqbM6ngzsTCRC8",
      name: " Samsung",
      partnershipForm: {
        index: 2,
        label: "Продакт плейсмент",
        value: "Продакт плейсмент",
      },
    },
  ],
  geography: [{ index: 1, label: "Россия", value: "Россия" }],
  reference: [{ id: "6iKHtjPJCaMCWkLKXu7jI" }],
  importance:
    "Проект позволит создать за короткие сроки широкую линейку молодежного контента, рассказанного на актуальном для аудитории языке. ",
  description:
    "Альманах из 10 полнометражных фильмов в скринлайф формате для молодой аудитории (14-20 лет) про жизнь подростков в интернете. Фильмы в разных жанрах, на разные темы, но с пересекающимися героями. Фильмы созданы режиссерами-дебютантами  и учениками образовательной программы Скринлайф. ",
  projectName: "Скринлайф Альманах",
  templateLang: "ru",
};
