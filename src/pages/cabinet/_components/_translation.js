import { translation } from "store/lang";
const dict =   {
    NEW_FOLDER: {en: 'New folder', ru: 'Новая папка'},
    NEW_FOLDER_NAME: {en:'New folder name', ru:'Название новой папки'},
    TYPE_NAME: {en:"Type the name", ru:"Введите название"},
    CANCEL:{en:'Cancel', ru:'Отмена'},
    CREATE: {en:'Create', ru:'Создать'}, 
    RENAME: {en:'Rename', ru:'Переименовать'},
    DELETE: {en:"Delete", ru:"Удалить"},
    CONFIRM_DELETE: {en:'Are you want to delete folder', ru:'Удалить папку'},
    EDIT_FOLDER_NAME: {en:'New folder name', ru:'Новое название папки'},
    SORT_BY:{en:'Sort by', ru:'Сортировать по'},
    NAME:{en:'name', ru:'имени'},
    CREATED_DATE:{en:'create date', ru:'дате создания'},
    UPDATED_DATE:{en:'update date', ru:'дате обновления'},
}

const text = translation(dict);
export default text