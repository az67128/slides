import { translation } from "store/lang";
const dict =  {
  MY_SLIDES: { en: "My slides", ru: "Мои презентации" },
  NEW_TEMPLSTES: { en: "New templates", ru: "Новые шаблоны" },
  MAIN: { en: "Main", ru: "Главная" },
  TEMPLATES: { en: "Templates", ru: "Шаблоны" },
  MY_SLIDES: { en: "My slides", ru: "Мои презентации" },
  ALL_TEMPLATES: { en: "All templates", ru: "Все шаблоны" },
  CREATE_SLIDES: { en: "Create slides", ru: "Создать презентацию" },
  NO_SLIDES: {en:'You don\'t have slides yet', ru:'У вас еще нет презентаций'},
  SEARCH: {en:'Search', ru:"Поиск"},
  NOTHING_FOUND: {en: 'Nothing found', ru:'Ничего не найдено'},
  MAIN: {en: 'Main', ru:"Главная"},
  FOLDER_IS_EMPTY: {en:'Folder is empty', ru:'Папка пуста'},
  ROOT_FOLDER_NAME: {en:'Root folder', ru:'Корневая папка'}
};

const text = translation(dict);
export default text