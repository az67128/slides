import { translation } from "store/lang";
const dict = {
  SAVE_FAILED: {
    en: "Can't save 😔 Try to save manually",
    ru: "Нам не удалось сохранить презентацию 😔 Попробуйте сохранить вручную",
  },
  ABOUT: { en: "About", ru: "О проекте" },
  NAME: { en: "Name", ru: "Название" },
  PROJECT_FETCH_ERROR: {
    en: "Error while fetching the project",
    ru: "Ошибка загрузки проекта",
  },
  SAVING: { en: "Saving", ru: "Сохраняем" },
  SAVED: { en: "Saved", ru: "Сохранено" },
};
const text = translation(dict);
export default text;
