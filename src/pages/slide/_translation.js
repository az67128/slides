import { translation } from "store/lang";
const dict = {
  ACCESS_DENIED: {
    en: "Slides are not available or owner restricted access",
    ru: "Презентация недоступна или пользователь запретил публичный показ",
  },
  ALL_TEMPLATES:{ en:'All templates', ru:'Все шаблоны'},
  HOME:{en:'Main page', ru:'На главную'},
  SELECT_TEMPLATE_FOR_CREATION: {en:'Select template for a new slide:', ru: 'Выберите шаблон презентации:'},
  COPY_SLIDE_HINT: {en:'Click the "Edit" button in the menu above to copy the presentation to your account', ru: 'Нажмите кнопку «Редактировать» в меню выше, чтобы скопировать презентацию в свою учетную запись'},
  FAVORITES: {en:'Favorites', ru:"Избранное"},
  REQUEST_TEMPLATE: {en:'Request a new template', ru:"Заказать свой шаблон"}
};

const text = translation(dict);

export default text;
