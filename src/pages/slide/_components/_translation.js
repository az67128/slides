import { translation } from "store/lang";
const dict = {
 
  NEW_PROJECT_NAME: {
    en: "The name of new file",
    ru: "Название нового файла",
  },
  TYPE_NAME: { en: "Type the name", ru: "Введите название" },
  CANCEL: { en: "Cancel", ru: "Отмена" },
  CREATE: { en: "Create", ru: "Cоздать" },
  
};
const text = translation(dict);
export default text
