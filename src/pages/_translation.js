import { translation } from "store/lang";
const dict =  {
    TITLE: { 
        en: "Create effective slides in 1 minute", 
        ru: "Создавай эффективные презентации за 1 минуту" 
    },
    SUBTITLE: { 
        en: "We created a slides generator to help you focus on the content", 
        ru: "Мы создали генератор презентаций, который поможет сконцентрироваться на содержании" 
    },
    PATTERNS: { en: "Over 20 patterns", ru: "Более 20 шаблонов" },
    TEAMWORK: { en: "An entire team can create", ru: "Создавайте целой командой" },
    DOWNLOAD: { en: "Download presentations in PDF and MP4", ru: "Скачивайте презентацию в PDF и MP4" },
    EXPORT: { en: "Export to Google presentations", ru: "Экспортируйте в Google презентации" },
    CREATE_SLIDES: { en: "Create slides", ru: "Создать презентацию" },
    MAIN: { en: "Main", ru: "Главная" },
    TEMPLATES: { en: "Templates", ru: "Шаблоны" },
    MY_SLIDES: { en: "My slides", ru: "Мои презентации" },
    PRICE: { en: "Price", ru: "Тарифы" },
    SIGN_IN: { en: "Login", ru: "Войти" },
    SIGN_UP: { en: "Sign up", ru: "Регистрация" },
    SIGN_IN_WITH: { en: "Login with", ru: "Войти с помощью" },
    SIGN_IN_TITLE: { en: "Login Teasy.one", ru: "Войти в Teasy.one" },
    SIGN_UP_TITLE: { en: "Sign up Teasy.one", ru: "Регистрация в Teasy.one" },
    CONFIRM_TITLE: { en: "Confirm email", ru: "Подтвердите email" },
    CONFIRM_BUTTON: { en: "Email confirmed", ru: "Email подтвержден" },
    OR: { en: "or", ru: "или"},
    LOGIN: { en: "Nickname", ru: "Никнейм"},
    EMAIL: { en: "Your email", ru: "Ваш  email"},
    PASSWORD: { en: "Your password", ru: "Ваш пароль"},
    FIRST_NAME: { en: "First Name", ru: "Имя"},
    LAST_NAME: { en: "Last Name", ru: "Фамилия"},
    FORGOT_PASS:{en:'Forgot password?', ru:'Забыли пароль?'},
    RESTORE_PASS: {en:'Password recovery', ru:'Восстановление пароля'},
    SEND_RESET_INSTRUCTIONS: {en:'Send reset instructions', ru:'Отправтиь инструкцию'},
    EMAIL_SENT: {en:'Email sent, please check your inbox', ru:'Email отправлен'},
    ENTER_NEW_PASS: {en:'Enter new password', ru:'Введите новый пароль'},
    NEW_PASS:{en:'New password', ru:"Новый пароль"},
    CONFRIM:{en:'Confirm', ru:'Подтвердить'},
    POSTER_URL:{en:'images/posteren.jpg', ru:'images/Poster.jpg'},
    CHECK_YOUR_INBOX: {en:'A link was sent to the specified email address to confirm registration', ru:'На указанный адрес электронной почты была отправлена ссылка для подтверждения регистрации'},
}
const text = translation(dict);
export default text
