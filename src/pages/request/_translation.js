import { translation } from "store/lang";
const dict = {
  SEND: {
    en: "Send request",
    ru: "Отправить заявку",
  },
  REQUEST_SENT: {
    en: "Request send",
    ru: "Заявка отправлена",
  },
  GO_BACK: {
    en: "Go back",
    ru: "Вернуться",
  },
  MANAGER_WILL_CONTACT_YOU: {
    en: "Our manager will contact you soon",
    ru: "Наш менеджер скоро свяжется с вами",
  }
};

const text = translation(dict);

export default text;
